import { Component, OnInit } from '@angular/core';
import { PolarisService } from '../polaris.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { engineer } from './engineerModel';
import { MatDialog } from '@angular/material';
import { EngineerDialogComponent } from '../engineer-dialog/engineer-dialog.component';
@Component({
  selector: 'app-engineer',
  templateUrl: './engineer.component.html',
  styleUrls: ['./engineer.component.css']
})
export class EngineerComponent implements OnInit {

  constructor(private polarisService:PolarisService, private formBulder:FormBuilder,private dialog:MatDialog) { }

  formGroup:FormGroup;

  ngOnInit() {

    this.formGroup = this.formBulder.group({
    
      engineerId:[""],
      experience:[""]

    })

    this.getEngineer();
    
  }

  engineer:any;

  getEngineer(){

  this.polarisService.getEngineer().subscribe(rs => this.engineer = rs)

  console.log(this.engineer)

  }

  edit:boolean = false;

  editEngineer(){

    this.edit = true;


  }

  updateEngineer(engineerId:number){

    let engineer:engineer ={

      engineerId:engineerId,
      firstName:"",
      lastName:"",
      experience:this.formGroup.get("experience").value

    }

    this.polarisService.updateEngineer(engineer);


  }

  openDialog(){

    this.dialog.open(EngineerDialogComponent,{

      width:'250px',
      height:'250px'

    })


  }


  deletEngineer(engineerId:number){

    this.polarisService.deleteEngineer(engineerId);
    
    

  }

}
