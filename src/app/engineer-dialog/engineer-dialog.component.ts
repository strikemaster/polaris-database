import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { engineer } from '../engineer/engineerModel';
import { MatDialogRef, MAT_TABS_CONFIG } from '@angular/material';
import { PolarisService} from '../polaris.service';
import { browser } from 'protractor';
@Component({
  selector: 'app-engineer-dialog',
  templateUrl: './engineer-dialog.component.html',
  styleUrls: ['./engineer-dialog.component.css']
})
export class EngineerDialogComponent implements OnInit {

  formGroup:FormGroup;

  constructor(private polaris:PolarisService, private formbuilder:FormBuilder, private dialog:MatDialogRef<EngineerDialogComponent>) { }

  ngOnInit() {

    this.formGroup = this.formbuilder.group({
 
      engineerId:[],
      firstName:[],
      lastName:[],
      experience:[]
    

    })

  }

  choice:number;

  saveEngineer(){


    let engineer:engineer = {

      engineerId:this.formGroup.get("engineerId").value,
      firstName:this.formGroup.get("firstName").value,
      lastName:this.formGroup.get("lastName").value,
      experience:this.formGroup.get("experience").value


    }


   this.polaris.addEngineer(engineer);

    location.reload();
    



  }

  routeIndex(index:number){

    console.log(index)


  }


}
