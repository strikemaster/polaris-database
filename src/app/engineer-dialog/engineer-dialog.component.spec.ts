import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerDialogComponent } from './engineer-dialog.component';

describe('EngineerDialogComponent', () => {
  let component: EngineerDialogComponent;
  let fixture: ComponentFixture<EngineerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
