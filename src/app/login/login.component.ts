import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { PolarisService } from '../polaris.service';
import { HttpClient } from '@angular/common/http';
import { login } from './loginModel';
import { users } from '../usersModel';
import { Time } from '@angular/common';
import { ArrayDataSource } from '@angular/cdk/collections';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup:FormGroup;

  user:any


  date:Date = new Date();

 

  constructor(private formBuilder:FormBuilder, private polaris:PolarisService,private http:HttpClient) { }

  ngOnInit() {
   
    this.formGroup = this.formBuilder.group({

      username:['', Validators.required],
      password:['', Validators.required]

    })

   
    
    
  }



  userDetails:boolean


  userValidation(){

    if(this.formGroup.get("username").invalid){
      return true;
    }else{
      return false
    }

  }

  passwordValidation(){

    if(this.formGroup.get("password").invalid){
     return true;
    }else{
      return false;
    }
  }





  authenticateDetails:login

  userName:string;
  password:string;

  authState:boolean;

  authentic(){

  

   
    this.authenticateDetails = {

     userName:this.formGroup.get("username").value,

     password:this.formGroup.get("password").value

    }


    
    this.polaris.getStatus(this.authenticateDetails).subscribe(rs => this.userDetails = rs );
    

    setTimeout(()=>{
  
      console.log(this.userDetails)

    if(this.userDetails == true){
 
      localStorage.setItem("key",this.formGroup.get("password").value)

    }else if(this.userDetails == false){

      this.authState = false;

    }

    },3000);
   
  }

   
  signOut(){

    
    localStorage.removeItem("key");
   
 
  
  }


    



  

 



 

  
}
