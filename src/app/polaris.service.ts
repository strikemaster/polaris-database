import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fleet } from './fleet/fleetModel';
import { crew } from './crew/crewModel';


import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PolarisService {

  url:string = "http://localhost:8080";

  constructor(private http:HttpClient) { }

 
//login//

  getStatus(details:any):Observable<any>{

    return this.http.post(this.url + "/users",details);
  }


  
  signedIn():boolean{

    if(localStorage.getItem("key")!= null){

      return true;


    }else
    {

            return false;
          }
  
        }     
 

  
  //retrieve fleet//

  getFleet():Observable<fleet>{

    let fleet:Observable<any> = this.http.get(this.url + "/fleet");
    
    console.log(fleet);

    return fleet;

  }

  //add fleet//

  addPlane(plane:any){

   

  this.http.post(this.url + "/plane",plane).subscribe();

  }

  //update fleet

  editFleet(plane:any){

    console.log(plane)

    this.http.put(this.url + "/edit",plane).subscribe();




  }

  deleteFleet(fleetId:any){

   

    this.http.delete(this.url + "/fleet" + "/" + fleetId).subscribe();

  }

  // get crew//
  getCrew():Observable<crew>{

    let crew:Observable<any> = this.http.get(this.url + "/crew") 

    return crew;

  }

  //add crew
  addCrew(crew:any){

    this.http.post(this.url + "/addCrew",crew).subscribe();




  }

  //update crew//

  updateCrew(crew:any){

    console.log(crew)

 this.http.put(this.url + "/addCrew",crew ).subscribe();


  }
  deleteCrew(crewId:any){

    this.http.delete(this.url + "/crew" + "/" + crewId).subscribe();
    
  }


  //get engineer//

  getEngineer(){


    let engineer:Observable<any> = this.http.get(this.url + "/engineer")

    return engineer;

  }

  //add engineer

  addEngineer(engineer:any){

  this.http.post(this.url + "/addEngineer",engineer).subscribe();

  }

  //update engineer//

  updateEngineer(engineer:any){

    this.http.put(this.url + "/editEngineer",engineer).subscribe();
  }


  deleteEngineer(engineerId:any){

    this.http.delete(this.url + "/engineer" + "/" + engineerId).subscribe();
    
  }


  // get route//

  getRoute(){

    let route:Observable<any> = this.http.get(this.url + "/route")

    return route

  }

  addRoute(route:any){

   this.http.post(this.url + "/addRoute",route).subscribe();

  }

  updateRoute(route:any){

    console.log(route)

    this.http.put(this.url + "/editRoute",route).subscribe();

  }
  
  deleteRoute(routeId:any){

    this.http.delete(this.url + "/route" + "/" +routeId).subscribe();

    
  }

  


}
