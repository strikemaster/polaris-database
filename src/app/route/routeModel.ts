export class route{

    routeId:number;
    routeOrigin:string;
    routeDestination:string;
    originAirport:string;
    destinationAirport:string;
    planeId:number;
    
}