import { Component, OnInit } from '@angular/core';
import { PolarisService } from '../polaris.service';
import { MatDialog } from '@angular/material';
import { RouteDialogComponent } from '../route-dialog/route-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { route } from './routeModel';
@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css']
})
export class RouteComponent implements OnInit {

  constructor(private polarisService:PolarisService, public dialog:MatDialog, private formBuilder:FormBuilder) { }

  route:any;

  formGroup:FormGroup;

  ngOnInit() {

    this.formGroup = this.formBuilder.group({
     
      routeId:[""],
      originAirport:[""],
      destinationAirport:[""]


    })



    this.getRoute();
  }

  getRoute(){

  this.polarisService.getRoute().subscribe(rs => this.route = rs);

  }

  openDialog():void{

    this.dialog.open(RouteDialogComponent, {

      width:'250px',
      height:'250px'

      
    })

  }


 

  editRoute(routeId:number){

    let route:route = {

      routeId:routeId,
      routeOrigin:null,
      routeDestination:null,
      originAirport:this.formGroup.get("originAirport").value,
      destinationAirport:this.formGroup.get("destinationAirport").value,
      planeId:0


    }

  this.polarisService.updateRoute(route);


  }

  edit:boolean = false;

  showEdit(){

    this.edit = true;

  }

  deleteRoute(routeId:number){

    this.polarisService.deleteRoute(routeId);

    location.reload(true);
    
  }



}
