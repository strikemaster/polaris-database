import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PolarisService } from '../polaris.service';
import { Observable } from 'rxjs';
import { crew } from './crewModel'
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material'
import { CrewDialogComponent } from '../crew-dialog/crew-dialog.component';
@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  constructor(private polarisService:PolarisService, private formBuilder:FormBuilder, private dialog:MatDialog) { }

 formGroup:FormGroup;

  ngOnInit() {

    this.formGroup = this.formBuilder.group({

      crewId:[""],
      position:[""],
      flightExperience:[""]



    })


    this.getCrew();

  }

  crew:any

  getCrew():void{

   this.polarisService.getCrew().subscribe(rs => this.crew = rs)

  }


  edit:boolean = false;

  editCrew(){

    this.edit = true;


  }

  updateCrew(crewId:number){

    let crew:crew = {

      crewId:crewId,
      firstName:"",
      lastName:"",
      position:this.formGroup.get("position").value,
      flightExperience:this.formGroup.get("flightExperience").value

    }

    this.polarisService.updateCrew(crew);


  }

  openDialog(){

this.dialog.open(CrewDialogComponent,{
 
  width:'250px',
  height:'250px'


})

  }

  deleteCrew(crewId:number){

    this.polarisService.deleteCrew(crewId);

   
    
  }



}
