import { Component, OnInit } from '@angular/core';
import { PolarisService } from '../polaris.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { fleet } from '../fleet/fleetModel';
import { FleetDialogComponent } from '../fleet-dialog/fleet-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-fleet',
  templateUrl: './fleet.component.html',
  styleUrls: ['./fleet.component.css']
})
export class FleetComponent implements OnInit {


 formGroup:FormGroup;

  fleet:fleet;

  url:string = "http://localhost:8080";

  constructor(private polarisService:PolarisService, private http:HttpClient, public dialog:MatDialog, private formBuilder:FormBuilder) { }

  ngOnInit() {


    this.formGroup = this.formBuilder.group({
     
      planeId:[""],
      capacity:[""],
      miles:[""]

    })

    //this.formGroup.disable();
     
   this.retrieveFleet();
   
  }

  retrieveFleet():void{

    this.polarisService.getFleet().subscribe(rs => this.fleet = rs);

    
  }

  openDialog():void{

   this.dialog.open(FleetDialogComponent,{

      width:'250px',
      height:'250px'
    });

  
  }

  edit:boolean = false;

  removeEdit:boolean = true;

  editFleet(){

 this.edit = true;

 this.removeEdit = false;
   


  }

  

  updateFleet(planeId:number){
 
    let fleet:fleet ={

      planeId:planeId,
      make:null,
      model:null,
      capacity:this.formGroup.get("capacity").value,
      miles:this.formGroup.get("miles").value,
      crewId:null,
      engineerId:null

    }

    this.polarisService.editFleet(fleet);

    location.reload(true);

  }
  
  deleteFleet(fleetId:number){

    this.polarisService.deleteFleet(fleetId);

    location.reload(true);
    
  }
  


}
