export class fleet{

    planeId:number;
    make:string;
    model:string;
    capacity:number;
    miles:number;
    crewId:number;
    engineerId:number;
}