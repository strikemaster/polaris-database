import { TestBed } from '@angular/core/testing';

import { PolarisService } from './polaris.service';

describe('PolarisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PolarisService = TestBed.get(PolarisService);
    expect(service).toBeTruthy();
  });
});
