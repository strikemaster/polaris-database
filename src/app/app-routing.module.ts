import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { FleetComponent } from './fleet/fleet.component';
const routes: Routes = [
  {path:"navigation", component:NavigationComponent},
  {path:"fleet",component:FleetComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
