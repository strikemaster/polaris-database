import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { crew } from '../crew/crewModel';
import { PolarisService } from '../polaris.service';
@Component({
  selector: 'app-crew-dialog',
  templateUrl: './crew-dialog.component.html',
  styleUrls: ['./crew-dialog.component.css']
})
export class CrewDialogComponent implements OnInit {

  formGroup:FormGroup;

  constructor(private polaris:PolarisService ,private dialog:MatDialogRef<CrewDialogComponent>, private formBuilder:FormBuilder) { }

  ngOnInit() {

    this.formGroup = this.formBuilder.group({
     
      crewId:[""],
      firstName:[""],
      lastName:[""],
      position:[""],
      flightExperience:[""]

    });

  }

  saveCrew(){

let crew:crew = {

  crewId:this.formGroup.get("crewId").value,
  firstName:this.formGroup.get("firstName").value,
  lastName:this.formGroup.get("lastName").value,
  position:this.formGroup.get("position").value,
  flightExperience:this.formGroup.get("flightExperience").value


}



 this.polaris.addCrew(crew);



  }

  closeDialog(){

    this.dialog.close();

    location.reload();
    
  }

 

}
