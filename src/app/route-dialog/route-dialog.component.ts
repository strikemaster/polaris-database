import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { route } from '../route/routeModel';
import { PolarisService } from '../polaris.service';
@Component({
  selector: 'app-route-dialog',
  templateUrl: './route-dialog.component.html',
  styleUrls: ['./route-dialog.component.css']
})
export class RouteDialogComponent implements OnInit {

  formGroup:FormGroup;

  constructor(private polaris:PolarisService, public dialog:MatDialogRef<RouteDialogComponent>,private formBuilder:FormBuilder) { }

  ngOnInit() {

    this.formGroup = this.formBuilder.group({

      routeId:[""],
      routeOrigin:[""],
      routeDestination:[""],
      originAirport:[""],
      destinationAirport:[""],
      planeId:[""]


    })
    

  }


  saveRoute(){

    let route:route = {


    routeId:this.formGroup.get("routeId").value,
    routeOrigin:this.formGroup.get("routeOrigin").value,
    routeDestination:this.formGroup.get("routeDestination").value,
    originAirport:this.formGroup.get("originAirport").value,
    destinationAirport:this.formGroup.get("destinationAirport").value,
    planeId:this.formGroup.get("planeId").value,

      


    }

    this.polaris.addRoute(route);

   
  }

closeDialog(){

this.dialog.close()

location.reload();

}



}
