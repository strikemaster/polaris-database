import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginComponent } from './login/login.component';
import {MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import { FleetComponent } from './fleet/fleet.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CrewComponent } from './crew/crew.component';
import { EngineerComponent } from './engineer/engineer.component';
import { RouteComponent } from './route/route.component';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import { FleetDialogComponent } from './fleet-dialog/fleet-dialog.component';
import { RouteDialogComponent } from './route-dialog/route-dialog.component';
import {MatTableModule} from '@angular/material/table';
import { MatMenuModule} from '@angular/material/menu';
import {MatCardModule} from '@angular/material/card';
import { CrewDialogComponent } from './crew-dialog/crew-dialog.component';
import { EngineerDialogComponent } from './engineer-dialog/engineer-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    FleetComponent,
    CrewComponent,
    EngineerComponent,
    RouteComponent,
    FleetDialogComponent,
    RouteDialogComponent,
    CrewDialogComponent,
    EngineerDialogComponent,
    
  ],
  
  entryComponents: [
    FleetDialogComponent,
    RouteDialogComponent,
    CrewDialogComponent,
    EngineerDialogComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonModule,
   MatExpansionModule,
   MatDialogModule,
   FormsModule,
   MatTableModule,
   MatMenuModule,
   MatCardModule,
   MatFormFieldModule
   

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
