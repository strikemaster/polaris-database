import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatDialogRef } from '@angular/material';
import { fleet } from '../fleet/fleetModel';
import { PolarisService } from '../polaris.service';
@Component({
  selector: 'app-fleet-dialog',
  templateUrl: './fleet-dialog.component.html',
  styleUrls: ['./fleet-dialog.component.css']
})
export class FleetDialogComponent implements OnInit {

  fg:FormGroup;

  id:number;
  make:Text;

  constructor(private fb:FormBuilder, public dialog:MatDialogRef<FleetDialogComponent>,private polarisService:PolarisService){ }

  ngOnInit() {
 
  this.fg = this.fb.group({

    planeId:[""],

    make:[""],

    model:[""],

    capacity:[""],

    miles:[""],

   


  })

  }



   addPlane(){

    let plane:fleet = {

      planeId:this.fg.get("planeId").value,

       make:this.fg.get("make").value,

       model:this.fg.get("model").value,

       capacity:this.fg.get("capacity").value,

       miles:this.fg.get("miles").value,

       crewId:0,

       engineerId:0
    }

    

    this.polarisService.addPlane(plane);
    
  }

  closeDialog():void{

    this.dialog.close();

   // location.reload();
  }


}
