import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetDialogComponent } from './fleet-dialog.component';

describe('FleetDialogComponent', () => {
  let component: FleetDialogComponent;
  let fixture: ComponentFixture<FleetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
